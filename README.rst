===============
**Twitnerário**
===============
**Get reminders of when your bus will pass.**

*Open source project - November 2011*

- `http://twitnerario.herokuapp.com/ <http://twitnerario.herokuapp.com/>`_
- `Sequence Diagram <https://docs.google.com/drawings/d/18vYGw2lVbu3fHChqzUnWq6aKX376Mju7tAPwHr1-vKg/edit>`_
- Python - Django Web application
- Twitter API
- Integrated with Vitória (Brazil) GPS bus system


Code `morenopc <https://github.com/morenopc>`_


Design `blude <https://github.com/blude>`_

Please feel free to contribute!
