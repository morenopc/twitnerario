from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template
from django.conf import settings
from django.contrib import admin
from twitterauth.views import twitter_signin, twitter_return
from core.tweet import localizar, pontos, linhas, send_tweets
from registros.views import pesquisar
admin.autodiscover()

urlpatterns=patterns('',
    url(r'^admin/', include(admin.site.urls)),
    
    #url(r'^pesquisar/', pesquisar, name='pesquisar'),
    url(r'^$', pesquisar, name='pesquisar'),
    url(r'^registro/', include('registros.urls', namespace='registro')),
    url(r'^login/$', twitter_signin, name='login'),
    url(r'^return/$', twitter_return, name='return'),
    url(r'^localizar/(.+)/$', localizar, name='localizar'),
    url(r'^pontos/$', pontos, name='pontos'),
    url(r'^(\d+)/linhas/$', linhas, name='linhas'),
    url(r'^facebookauth/$', direct_to_template,
        {'template': 'facebook_auth/facebook_auth.html'},
        name='facebook_auth'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
            { 'document_root': settings.MEDIA_ROOT }),
    )
